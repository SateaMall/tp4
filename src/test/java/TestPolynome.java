import org.junit.jupiter.api.Test;
import polynomes.Monome;
import polynomes.Polynome;

import java.util.ArrayList;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.fail;
public class TestPolynome {
    @Test
    public void testJSP(){
        Polynome P1=new Polynome();

        Monome m0=new Monome(5,0);
        Monome m1=new Monome(0,1);

        Monome m4=new Monome(3,4);
        Monome m5=new Monome(3,4);

        P1.ajoutMonome(m0);
        P1.ajoutMonome(m1);

        //assertEquals("+5",P1.toString());
        P1.ajoutMonome(m4);
        P1.ajoutMonome(m5);

        //assertEquals(P1.toString(),"+5+6x^4");
        System.out.println(P1.toString());


    }
}
