import org.junit.jupiter.api.Test;
import polynomes.DegreNegatifException;
import polynomes.Monome;

import static junit.framework.Assert.fail;

public class TestMonome {
    @Test
    public void testToString() {
        Monome M1 = new Monome(4, 0);
        Monome M2 = new Monome(4, 1);

        assert (M1.toString().equals( "+4"));
        assert (M2.toString().equals("+4x"));
    }

    @Test
    public void testDegreNegatif() {
        try {
            Monome  M2 = new Monome(4,-1);
            fail("dfd"); // il a ignoré le fait et c'est bien
        }
        catch(DegreNegatifException e){
            assert(true);
        }
    }
}
